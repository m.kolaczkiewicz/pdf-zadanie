import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ApachePDFBox {
    public static void main(String[] args) throws IOException {
        String sprzedawca, nabywca, termin, pozycje;
        PDFManager pdfManager = new PDFManager();
        pdfManager.setFilePath("C:\\faktura1.pdf");
        String example = pdfManager.ToText();
        sprzedawca = example.substring(example.lastIndexOf("Nabywca:") + 8, example.indexOf("NIP") + 14);
        nabywca = example.substring(example.indexOf("NIP") + 14, example.lastIndexOf("NIP") + 14);
        termin = example.substring(example.indexOf("Termin płatności:") + 18, example.lastIndexOf("Sprzedawca"));
        pozycje = example.substring(example.indexOf("Stawka") + 13, example.lastIndexOf("PODSUMOWANIE"));
        System.out.print("Sprzedawca:" + sprzedawca + "\nNabywca:" + nabywca +
                "\nTermin Płatności:\n" + termin + "\nPozycje: (ilość/cena jedn./wartość/VAT)\n" + pozycje);
    }
}
